section .text

%define SYSEXIT 60
%define SYSWRITE 1
%define STDOUT 1
%define NEWLINE_SYMBOL 0xA
%define BIT_DIVIDER 10
%define ACII_NUMBERS_CODE 0x30
%define MINUS_SYMBOL "-"
%define SUCCESS 1
%define FAIL 0
%define SPACE_SYMBOL 0x20
%define TAB_SYMBOL 0x9
%define NULLTERMINATED_SYMBOL 0
 
; Принимает код возврата и завершает текущий процесс
exit:
    xor rdi, rdi
    mov rax, SYSEXIT ; останов
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ; 0 в rax
    .cycle: ; цикл счетчика длины
        cmp byte [rdi+rax], NULLTERMINATED_SYMBOL  ; выставление флагов по результату вычитания из кода символа, стоящего на rax  позиции в строке,  нуля
        je .end ; if zf=1 goto .end
        inc rax ; инкрементация значения в rax
        jmp .cycle ; возврат к началу тела цикла
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi ; в стек отправляем rdi
    call string_length ; высчитываем длину строки
    pop rsi ;  в rsi кладем указатель на строку из стека
    mov rdx, rax ; в rdx кладем длину строки 
    mov rax, SYSWRITE ; в rax  1 системный номер write
    mov rdi, STDOUT  ; дескриптор stdout
    syscall
    mov rdi, rsi ; в  rdi возвращаем предыдущее значение
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi ; в стек отправляем rdi
    mov rsi, rsp ; в rsi адрес кода символа
    mov rdi, STDOUT ; дескриптор stdout
    mov rdx, 1 ; длина
    mov rax, SYSWRITE ; в rax  1 системный номер write
    syscall
    pop rdi ; в rdi возвращаем предыдущее значение
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_SYMBOL ; кладем код символа перевода строки
    jmp print_char ; вызов вывода символа

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; побитовое И, провераем на отрицательность
    js .negative  ; if sf=1 goto .negative
    jmp print_uint ; число беззнаковое, уху, вызов вывода беззнакового
    .negative: ; печать знакового
    push rdi ; в стек rdi 
    mov rdi, MINUS_SYMBOL ; знак - печатаем
    call print_char 
    pop rdi ; вернули значение rdi со стека
    neg rdi ; сделали число положительным

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi ; 
    mov r8, BIT_DIVIDER  ; код 10 в r8
    push NULLTERMINATED_SYMBOL ; 0 в стек для нультерминированной строки
    .split: ; число разбиваем на цифры
    xor rdx, rdx ; 0 в rdx
    div r8 ; rax / 10 -> rax
    add rdx, ACII_NUMBERS_CODE ; получаем код цифры 
    push rdx  ; код цифры пушим в стек
    test rax, rax ; логическое И и выставляем флаг zero
    jnz .split ; if zf!=0 goto .split
    .print: ; числа
    pop rdi ; из стека достаем код цифры
    test rdi, rdi ; проверка на 0
    jz .exit ; если 0, то это конец строки и на выход
    call print_char ; печать символа
    jmp .print ; переходим к печати следующей цифры
    .exit: ; на выход
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length ; 
    push rdi ; 
    push rax ; 
    mov rdi, rsi ; 
    call string_length ; 
    pop rcx 
    pop rsi
    cmp rcx,rax
    jne .fail
    .cycle:
    cmp rcx, -1
    je .success
    mov r8b, byte[rdi+rcx]
    mov r9b, byte[rsi+rcx]
    cmp r8, r9
    jne .fail
    dec rcx
    jmp .cycle
    .fail:
    mov rax, FAIL
    ret

    .success:
    mov rax, SUCCESS
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ; 0 системный номер чтения
    xor rdi, rdi ; 0 читаем из stdin
    push 0 ; выделили местечко для символа
    mov rsi, rsp ; читаем в стек
    mov rdx, 1 ; читаем 1 байт
    syscall
    pop rax ; возвращаем символ 
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi  ; адрес
    mov r9, rsi ; размер буфера
    xor r10, r10 ; счетчик длины слова 
    .whitespace: ; пробелы перед словом
    call read_char ; читаем символ
    cmp rax, NULLTERMINATED_SYMBOL ; конец строки? 
    je .exit ;  тогда на выход
    cmp rax, NEWLINE_SYMBOL  ; проверка на пробельный символ
    je .whitespace
    cmp rax, SPACE_SYMBOL ; проверка на пробельный символ
    je .whitespace
    cmp rax, TAB_SYMBOL  ; проверка на пробельный символ
    je .whitespace
    jmp .write ; переходим к write
    .read:
    call read_char
    cmp rax, NULLTERMINATED_SYMBOL
    je .exit
    cmp rax, SPACE_SYMBOL
    je .exit
    cmp rax, TAB_SYMBOL
    je .exit
    cmp rax, NEWLINE_SYMBOL
    je .exit
    jmp .write
    .write: 
    mov byte[r8+r10], al ; в буфер записываем 8 битов rax те символ
    inc r10 ; увеличиваем счетчик длины сллова
    cmp r10, r9 ; выставляем флаги по r10-r9
    jz .last ; if zf=0 goto .last
    jmp .read ; if zf!=0 goto .read
    .last:
    test rax, rax ; проверяем последний на 0
    jnz .fail ; if zf!=0 goto .fail
    jmp .exit ; он 0!!!! на выход
    .fail:
    mov rax, FAIL ; возвращаем 0
    ret
    .exit: 
    mov byte[r8+r10], NULLTERMINATED_SYMBOL ; 0
    mov rax, r8 ; адрес буфера
    mov rdx, r10 ;  возвращаем длину слова
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rdx, rdx
    xor rax, rax
    mov r8, 10
    .read:
    mov dl, byte[rdi+rcx]
    cmp rdx, ACII_NUMBERS_CODE
    jl .exit ; меньше 0, на выход
    cmp rdx, ACII_NUMBERS_CODE + 9
    jg .exit ; больше 9, на выход
    sub rdx, ACII_NUMBERS_CODE ; получаем цифру вместо кода 
    jmp .multiplication
    .multiplication: ; преобразование в число 
    push rdx
    mul r8 
    pop rdx
    add rax, rdx
    inc rcx
    jmp .read
    .exit:
    mov rdx, rcx
    ret 

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], MINUS_SYMBOL
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi 
    call string_length ; длина строки 
    pop rdi 
    cmp rax , rdx ; сравниваем длину строки с размером буфера
    jg .fail ;  если rax больше чем rdx, goto .fail
    .loop:
    mov r8b, byte[rdi] 
    mov byte[rsi], r8b
    inc rdi ; 
    inc rsi ;
    test r8b, r8b ; проверка на 0
    jnz .loop ; if zf!=0 goto .loop
    ret
    .fail: 
    mov rax, FAIL ; возврат 0 - ошибка
    ret
